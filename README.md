# Pandai

Pandai is short for **P**roject **A**ssistant with **N**etworking **D**aemon and
**A**rtificial **I**ntelligence.

It's a bunch of tools I made to help me organize my work.

## License

Pandai is licensed under the GPL-3.0.

## Building

We use the Meson (and thereby Ninja) build system for Pandai. The quickest
way to get going is to do the following:

```sh
meson . _build
ninja -C _build
ninja -C _build install
```

For build options see [meson_options.txt](./meson_options.txt).
