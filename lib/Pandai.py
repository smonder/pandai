#!/usr/bin/env python3
#
# Pandai.py
#
# Copyright 2022 Salim Monder <salim.monder@outlook.com>
#
# This file is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GObject
from gi.repository import Gio
import inspect

from ..importer import modules

Pandai = modules['Pandai']._introspection_module
__all__ = []

def _modname():
    import inspect
    frm = inspect.stack()[2]
    mod = inspect.getmodule(frm[0])
    return mod.__name__

def _log(domain, level, *messages):
    message = ' '.join(messages)
    v = GLib.Variant('a{sv}', {'MESSAGE': GLib.Variant.new_string(message)})
    GLib.log_variant(domain, level, v)

def critical(*messages):
    _log(_modname(), GLib.LogLevelFlags.LEVEL_CRITICAL, *messages)

def warning(*messages):
    _log(_modname(), GLib.LogLevelFlags.LEVEL_WARNING, *messages)

def debug(*messages):
    _log(_modname(), GLib.LogLevelFlags.LEVEL_DEBUG, *messages)

def message(*messages):
    _log(_modname(), GLib.LogLevelFlags.LEVEL_MESSAGE, *messages)

def info(*messages):
    _log(_modname(), GLib.LogLevelFlags.LEVEL_INFO, *messages)

Pandai.critical = critical
Pandai.debug = debug
Pandai.info = info
Pandai.message = message
Pandai.warning = warning}
