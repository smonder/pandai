/*
 * pandai-task-group-list.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiTaskGroupList"

#include "pandai-config.h"
#include "pandai-debug.h"

#include <glib/gi18n.h>

#include "pandai-enums.h"
#include "pandai-macros.h"
#include "pandai-types.h"

#include "pandai-task-row-private.h"
#include "pandai-task-group-list.h"

struct _PandaiTaskGroupList
{
  GtkWidget               parent_instance;

  PandaiTaskGroup *       group;

  /* -----< TEMPLATE WIDGETS >----- */
  AdwPreferencesPage *    group_page;
  GtkImage *              group_priority;
  GtkListBox *            tasks_list;

  AdwEntryRow *           group_edit_name;
  AdwComboRow *           group_edit_priority;
};

G_DEFINE_FINAL_TYPE (PandaiTaskGroupList, pandai_task_group_list, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_GROUP,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void        pandai_task_group_list_set_group   (PandaiTaskGroupList * self,
                                                       PandaiTaskGroup *     group);


static void
on_add_task_button_clicked_cb (PandaiTaskGroupList *self,
                               GtkButton           *btn)
{
  g_autoptr (PandaiTask) task = NULL;

  g_assert (PANDAI_IS_TASK_GROUP_LIST (self));
  g_assert (GTK_IS_BUTTON (btn));

  task = pandai_task_new ("new task");
  pandai_task_set_memo (task, "A newly created task\nWrite a task decription here.\n\n");

  pandai_task_group_add_task (self->group, g_steal_pointer (&task));
}

static void
pandai_task_group_list_finalize (GObject *object)
{
  PandaiTaskGroupList *self = (PandaiTaskGroupList *)object;

  g_clear_weak_pointer (&self->group);

  G_OBJECT_CLASS (pandai_task_group_list_parent_class)->finalize (object);
}

static void
pandai_task_group_list_dispose (GObject *object)
{
  PandaiTaskGroupList *self = (PandaiTaskGroupList *)object;
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))) != NULL)
    g_clear_pointer (&child, gtk_widget_unparent);

  G_OBJECT_CLASS (pandai_task_group_list_parent_class)->dispose (object);
}

static void
pandai_task_group_list_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  PandaiTaskGroupList *self = PANDAI_TASK_GROUP_LIST (object);

  switch (prop_id)
    {
    case PROP_GROUP:
      g_value_set_object (value, pandai_task_group_list_get_group (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_group_list_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  PandaiTaskGroupList *self = PANDAI_TASK_GROUP_LIST (object);

  switch (prop_id)
    {
    case PROP_GROUP:
      pandai_task_group_list_set_group (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_group_list_class_init (PandaiTaskGroupListClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = pandai_task_group_list_finalize;
  object_class->dispose = pandai_task_group_list_dispose;
  object_class->get_property = pandai_task_group_list_get_property;
  object_class->set_property = pandai_task_group_list_set_property;

  properties [PROP_GROUP] =
    g_param_spec_object ("group", NULL, NULL,
                         PANDAI_TYPE_TASK_GROUP,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_GROUP);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/pandai-widgets/pandai-task-group-list.ui");
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskGroupList, group_page);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskGroupList, group_priority);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskGroupList, tasks_list);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskGroupList, group_edit_name);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskGroupList, group_edit_priority);

  gtk_widget_class_bind_template_callback (widget_class, on_add_task_button_clicked_cb);
}

static void
pandai_task_group_list_init (PandaiTaskGroupList *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


/**
 * pandai_task_group_list_new:
 * @group: a #PandaiTaskGroup.
 *
 * Creates a new #PandaiTaskGroupList for @group.
 *
 * Returns: a newly created #GtkWidget or %NULL.
 */
GtkWidget *
pandai_task_group_list_new (PandaiTaskGroup *group)
{
  g_return_val_if_fail (PANDAI_IS_TASK_GROUP (group), NULL);

  return g_object_new (PANDAI_TYPE_TASK_GROUP_LIST,
                       "group", group,
                       NULL);
}

/**
 * pandai_task_group_list_get_group:
 * @self: a #PandaiTaskGroupList.
 *
 * Gets the #PandaiTaskGroup that is linked with @self.
 *
 * Returns: (transfer full): a #PandaiTaskGroup or %NULL.
 */
PandaiTaskGroup *
pandai_task_group_list_get_group (PandaiTaskGroupList *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK_GROUP_LIST (self), NULL);

  return self->group;
}

static GtkWidget *
create_task_row (GObject  *item,
                 gpointer  user_data)
{
  PandaiTask *task = PANDAI_TASK (item);

  g_assert (PANDAI_IS_TASK (task));

  return _pandai_task_row_new (task);
}

static void
pandai_task_group_list_set_group (PandaiTaskGroupList *self,
                                  PandaiTaskGroup     *group)
{
  g_return_if_fail (PANDAI_IS_TASK_GROUP_LIST (self));
  g_return_if_fail (PANDAI_IS_TASK_GROUP (group));

  if (g_set_weak_pointer (&self->group, group))
    {
      g_object_bind_property (self->group, "name",
                              self->group_page, "title",
                              G_BINDING_SYNC_CREATE);

      gtk_list_box_bind_model (self->tasks_list,
                               G_LIST_MODEL (self->group),
                               (GtkListBoxCreateWidgetFunc)create_task_row,
                               NULL, NULL);

      g_object_bind_property_full (self->group, "priority",
                                   self->group_priority, "icon-name",
                                   G_BINDING_SYNC_CREATE,
                                   pandai_priority_to_icon_value,
                                   NULL, NULL, NULL);

      g_object_bind_property (self->group, "name",
                              self->group_edit_name, "text",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

      g_object_bind_property (self->group, "priority",
                              self->group_edit_priority, "selected",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_GROUP]);
    }
}
