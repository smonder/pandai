/*
 * pandai-task-row.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiTaskRow"

#include "pandai-config.h"
#include "pandai-debug.h"

#include <glib/gi18n.h>

#include "pandai-macros.h"
#include "pandai-task-row-private.h"
#include "pandai-task-viewer.h"

struct _PandaiTaskRow
{
  AdwActionRow parent_instance;

  PandaiTask * task;

  /* -----< CHILD WIDGETS >----- */
  GtkImage *   task_priority_icon;
  GtkImage *   task_status_icon;
};

G_DEFINE_FINAL_TYPE (PandaiTaskRow, pandai_task_row, ADW_TYPE_ACTION_ROW)

enum {
  PROP_0,
  PROP_TASK,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
pandai_task_row_set_task (PandaiTaskRow *self,
                          PandaiTask    *task)
{
  g_return_if_fail (PANDAI_IS_TASK_ROW (self));
  g_return_if_fail (PANDAI_IS_TASK (task));

  if (g_set_object (&self->task, task))
    {
      g_object_bind_property (task, "name", self, "title",
                              G_BINDING_SYNC_CREATE);
      g_object_bind_property_full (task, "priority",
                                   self->task_priority_icon, "icon-name",
                                   G_BINDING_SYNC_CREATE,
                                   pandai_priority_to_icon_value, NULL, NULL, NULL);
      g_object_bind_property_full (task, "status",
                                   self->task_status_icon, "icon-name",
                                   G_BINDING_SYNC_CREATE,
                                   pandai_status_to_icon_value, NULL, NULL, NULL);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TASK]);
    }
}

static void
pandai_task_row_activated_cb (AdwActionRow *row,
                              gpointer      user_data)
{
  PandaiTaskRow *self = (PandaiTaskRow *)row;
  PandaiTaskViewer *viewer;
  GtkRoot *root;

  root = gtk_widget_get_root (GTK_WIDGET (self));

  viewer = (PandaiTaskViewer *)pandai_task_viewer_new ();
  pandai_task_viewer_set_task (viewer, self->task);

  if (GTK_IS_WINDOW (root))
    pandai_task_viewer_show_dialog (viewer, GTK_WINDOW (root));
  else
    g_error ("Critical Error. PandaiTaskRow is not contained in any window.");
}

static void
pandai_task_row_dispose (GObject *object)
{
  PandaiTaskRow *self = (PandaiTaskRow *)object;
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))) != NULL)
    g_clear_pointer (&child, gtk_widget_unparent);

  G_OBJECT_CLASS (pandai_task_row_parent_class)->dispose (object);
}

static void
pandai_task_row_finalize (GObject *object)
{
  PandaiTaskRow *self = (PandaiTaskRow *)object;

  g_clear_object (&self->task);

  G_OBJECT_CLASS (pandai_task_row_parent_class)->finalize (object);
}

static void
pandai_task_row_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  PandaiTaskRow *self = PANDAI_TASK_ROW (object);

  switch (prop_id)
    {
    case PROP_TASK:
      g_value_set_object (value, _pandai_task_row_get_task (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_row_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  PandaiTaskRow *self = PANDAI_TASK_ROW (object);

  switch (prop_id)
    {
    case PROP_TASK:
      pandai_task_row_set_task (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_row_class_init (PandaiTaskRowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = pandai_task_row_dispose;
  object_class->finalize = pandai_task_row_finalize;
  object_class->get_property = pandai_task_row_get_property;
  object_class->set_property = pandai_task_row_set_property;

  properties [PROP_TASK] =
    g_param_spec_object ("task", NULL, NULL,
                         PANDAI_TYPE_TASK,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pandai_task_row_init (PandaiTaskRow *self)
{
  self->task_priority_icon = g_object_new (GTK_TYPE_IMAGE,
                                           "valign", GTK_ALIGN_CENTER,
                                           "tooltip-text", _("Task priority level"),
                                           NULL);
  self->task_status_icon = g_object_new (GTK_TYPE_IMAGE,
                                         "valign", GTK_ALIGN_CENTER,
                                         "tooltip-text", _("Task status"),
                                         NULL);

  adw_action_row_add_suffix (ADW_ACTION_ROW (self), GTK_WIDGET (self->task_priority_icon));
  adw_action_row_add_suffix (ADW_ACTION_ROW (self), GTK_WIDGET (self->task_status_icon));

  gtk_list_box_row_set_activatable (GTK_LIST_BOX_ROW (self), TRUE);
  g_signal_connect (self, "activated", G_CALLBACK (pandai_task_row_activated_cb), NULL);
}


/**
 * _pandai_task_row_new:
 * @task: a #PandaiTask.
 *
 * Creates a new #PandaiTaskRow for @task.
 *
 * Returns: a newly created #GtkWidget or %NULL
 */
GtkWidget *
_pandai_task_row_new (PandaiTask * task)
{
  g_return_val_if_fail (PANDAI_IS_TASK (task), NULL);

  return g_object_new (PANDAI_TYPE_TASK_ROW,
                       "task", task,
                       NULL);
}


/**
 * _pandai_task_row_get_task:
 * @self: a #PandaiTaskRow.
 *
 * Gets the #PandaiTask object associated with @self.
 *
 * Returns: (transfer full): a #PandaiTask or %NULL.
 */
PandaiTask *
_pandai_task_row_get_task (PandaiTaskRow *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK_ROW (self), NULL);

  return self->task;
}
