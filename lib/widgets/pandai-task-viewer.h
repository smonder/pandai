/*
 * pandai-task-viewer.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(PANDAI_INSIDE) && !defined(PANDAI_COMPILATION)
#error "Only <libpandai.h> can be included directly."
#endif

#include <adwaita.h>

#include "pandai-task.h"
#include "pandai-types.h"
#include "pandai-version-macros.h"

G_BEGIN_DECLS

#define PANDAI_TYPE_TASK_VIEWER (pandai_task_viewer_get_type())

PANDAI_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (PandaiTaskViewer, pandai_task_viewer, PANDAI, TASK_VIEWER, GtkWidget)

PANDAI_AVAILABLE_IN_ALL
GtkWidget *  pandai_task_viewer_new         (void) G_GNUC_WARN_UNUSED_RESULT;

PANDAI_AVAILABLE_IN_ALL
PandaiTask * pandai_task_viewer_get_task    (PandaiTaskViewer * self);
PANDAI_AVAILABLE_IN_ALL
void         pandai_task_viewer_set_task    (PandaiTaskViewer * self,
                                             PandaiTask *       task);
PANDAI_AVAILABLE_IN_ALL
void         pandai_task_viewer_show_dialog (PandaiTaskViewer * self,
                                             GtkWindow *        parent_window);

G_END_DECLS
