/*
 * pandai-task-viewer.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiTaskViewer"

#include "pandai-config.h"
#include "pandai-debug.h"

#include <glib/gi18n.h>

#include "pandai-enums.h"
#include "pandai-macros.h"

#include "pandai-task-viewer.h"

struct _PandaiTaskViewer
{
  GtkWidget               parent_instance;

  PandaiTask *            task;

  /* -----< TEMPLATE WIDGETS >----- */
  AdwPreferencesPage *    task_page;

  AdwEntryRow *           task_name;
  AdwActionRow *          task_date_start;
  GtkLabel *              date_start_label;
  AdwActionRow *          task_due_date;
  GtkLabel *              due_date_label;
  AdwComboRow *           task_priority;
  AdwComboRow *           task_status;
  GtkTextView *           task_memo;
  GtkTextBuffer *         task_memo_buffer;

  GtkButton *             add_media;
};

G_DEFINE_FINAL_TYPE (PandaiTaskViewer, pandai_task_viewer, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_TASK,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
on_add_media_clicked_cb (PandaiTaskViewer *self,
                         GtkButton        *add_media)
{
  g_assert (PANDAI_IS_TASK_VIEWER (self));
  g_assert (GTK_IS_BUTTON (add_media));

  /* TODO: add media to the task here. */
}


static void
pandai_task_viewer_finalize (GObject *object)
{
  PandaiTaskViewer *self = (PandaiTaskViewer *)object;

  g_clear_weak_pointer (&self->task);

  G_OBJECT_CLASS (pandai_task_viewer_parent_class)->finalize (object);
}

static void
pandai_task_viewer_dispose (GObject *object)
{
  PandaiTaskViewer *self = (PandaiTaskViewer *)object;
  GtkWidget *child;

  while ((child = gtk_widget_get_first_child (GTK_WIDGET (self))) != NULL)
    g_clear_pointer (&child, gtk_widget_unparent);

  G_OBJECT_CLASS (pandai_task_viewer_parent_class)->dispose (object);
}

static void
pandai_task_viewer_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  PandaiTaskViewer *self = PANDAI_TASK_VIEWER (object);

  switch (prop_id)
    {
    case PROP_TASK:
      g_value_set_object (value, pandai_task_viewer_get_task (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_viewer_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  PandaiTaskViewer *self = PANDAI_TASK_VIEWER (object);

  switch (prop_id)
    {
    case PROP_TASK:
      pandai_task_viewer_set_task (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_viewer_class_init (PandaiTaskViewerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = pandai_task_viewer_finalize;
  object_class->dispose = pandai_task_viewer_dispose;
  object_class->get_property = pandai_task_viewer_get_property;
  object_class->set_property = pandai_task_viewer_set_property;

  properties [PROP_TASK] =
    g_param_spec_object ("task", NULL, NULL,
                         PANDAI_TYPE_TASK,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
  gtk_widget_class_set_accessible_role (widget_class, GTK_ACCESSIBLE_ROLE_GROUP);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/pandai-widgets/pandai-task-viewer.ui");
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, task_page);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, task_name);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, task_date_start);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, date_start_label);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, task_due_date);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, due_date_label);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, task_priority);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, task_status);
  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, task_memo);

  gtk_widget_class_bind_template_child (widget_class, PandaiTaskViewer, add_media);

  gtk_widget_class_bind_template_callback (widget_class, on_add_media_clicked_cb);
}

static void
pandai_task_viewer_init (PandaiTaskViewer *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->task_memo_buffer = gtk_text_view_get_buffer (self->task_memo);
  gtk_text_buffer_set_enable_undo (self->task_memo_buffer, TRUE);
}


/**
 * pandai_task_viewer_new:
 *
 * Creates a new #PandaiTaskView.
 *
 * Returns: a newly created #GtkWidget.
 */
GtkWidget *
pandai_task_viewer_new (void)
{
  return g_object_new (PANDAI_TYPE_TASK_VIEWER, NULL);
}

/**
 * pandai_task_viewer_get_task:
 * @self: a #PandaiTaskViewer.
 *
 * Gets the #BandaiTask of @self.
 *
 * Returns: (transfer full): A #PandaiTask or %NULL.
 */
PandaiTask *
pandai_task_viewer_get_task (PandaiTaskViewer *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK_VIEWER (self), NULL);

  return self->task;
}

void
pandai_task_viewer_set_task (PandaiTaskViewer *self,
                             PandaiTask       *task)
{
  g_return_if_fail (PANDAI_IS_TASK_VIEWER (self));
  g_return_if_fail (PANDAI_IS_TASK (task));

  PANDAI_ENTRY;

  if (self->task != task)
    {
      g_clear_weak_pointer (&self->task);
      g_set_weak_pointer (&self->task, task);

      g_object_bind_property (self->task, "name",
                              self->task_page, "title",
                              G_BINDING_SYNC_CREATE);

      g_object_bind_property (self->task, "name",
                              self->task_name, "text",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

      g_object_bind_property (self->task, "priority",
                              self->task_priority, "selected",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      g_object_bind_property (self->task, "status",
                              self->task_status, "selected",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      g_object_bind_property (self->task, "memo",
                              self->task_memo_buffer, "text",
                              G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
      g_object_bind_property_full (self->task, "date-start",
                                   self->date_start_label, "label",
                                   G_BINDING_SYNC_CREATE,
                                   pandai_date_time_to_str_value,
                                   pandai_date_time_from_str_value,
                                   NULL, NULL);
      g_object_bind_property_full (self->task, "due-date",
                                   self->due_date_label, "label",
                                   G_BINDING_SYNC_CREATE,
                                   pandai_date_time_to_str_value,
                                   pandai_date_time_from_str_value,
                                   NULL, NULL);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TASK]);
    }

  PANDAI_EXIT;
}

void
pandai_task_viewer_show_dialog (PandaiTaskViewer *self,
                                GtkWindow        *parent_window)
{
  AdwWindow *window;
  GtkBox *box;
  AdwHeaderBar *hbar;

  g_return_if_fail (PANDAI_IS_TASK_VIEWER (self));
  g_return_if_fail (GTK_IS_WINDOW (parent_window));

  PANDAI_ENTRY;

  box = g_object_new (GTK_TYPE_BOX,
                      "orientation", GTK_ORIENTATION_VERTICAL,
                      "spacing", 0,
                      NULL);

  hbar = g_object_new (ADW_TYPE_HEADER_BAR, NULL);
  gtk_widget_add_css_class (GTK_WIDGET (hbar), "flat");

  gtk_box_append (box, GTK_WIDGET (hbar));
  gtk_box_append (box, GTK_WIDGET (self));

  window = g_object_new (ADW_TYPE_WINDOW,
                         "default-height", 800,
                         "default-width", 600,
                         "modal", TRUE,
                         "transient-for", parent_window,
                         "content", box,
                         NULL);

  g_object_bind_property (self->task, "name",
                          window, "title",
                          G_BINDING_SYNC_CREATE);

  gtk_window_present (GTK_WINDOW (window));

  PANDAI_EXIT;
}
