/*
 * pandai-task-group.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiTaskGroup"

#include "pandai-config.h"
#include "pandai-debug.h"

#include "pandai-enums.h"
#include "pandai-task-group.h"
#include "pandai-macros.h"

struct _PandaiTaskGroup
{
  GObject         parent_instance;

  /* ----< TASK_GROUP PROPERTIES >---- */
  gchar *         name;
  PandaiPriority  priority;

  /* ----< TASK_GROUP DETAILED CONTENTS >---- */
  GPtrArray *     task_list;
  guint           n_items;
};

static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (PandaiTaskGroup, pandai_task_group, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

enum {
  PROP_0,
  PROP_NAME,
  PROP_PRIORITY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];
static GListModelInterface *parent_list_model_iface;

static GType
pandai_task_group_get_item_type (GListModel *list)
{
  return PANDAI_TYPE_TASK;
}

static guint
pandai_task_group_get_n_items (GListModel *list)
{
  PandaiTaskGroup *self = (PandaiTaskGroup *)list;

  g_assert (PANDAI_IS_TASK_GROUP (self));

  return self->n_items;
}

static gpointer
pandai_task_group_get_item (GListModel *list,
                       guint       position)
{
  PandaiTaskGroup *self = (PandaiTaskGroup *)list;

  g_assert (PANDAI_IS_TASK_GROUP (self));

  return g_ptr_array_index (self->task_list, position);
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  parent_list_model_iface = g_type_interface_peek_parent (iface);

  iface->get_item_type = pandai_task_group_get_item_type;
  iface->get_n_items = pandai_task_group_get_n_items;
  iface->get_item = pandai_task_group_get_item;
}


static void
pandai_task_group_finalize (GObject *object)
{
  PandaiTaskGroup *self = (PandaiTaskGroup *)object;

  g_clear_pointer (&self->name, g_free);
  g_ptr_array_free (self->task_list, TRUE);

  G_OBJECT_CLASS (pandai_task_group_parent_class)->finalize (object);
}

static void
pandai_task_group_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  PandaiTaskGroup *self = PANDAI_TASK_GROUP (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, pandai_task_group_get_name (self));
      break;

    case PROP_PRIORITY:
      g_value_set_enum (value, pandai_task_group_get_priority (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_group_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  PandaiTaskGroup *self = PANDAI_TASK_GROUP (object);

  switch (prop_id)
    {
    case PROP_NAME:
      pandai_task_group_set_name (self, g_value_get_string (value));
      break;

    case PROP_PRIORITY:
      pandai_task_group_set_priority (self, g_value_get_enum (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_group_class_init (PandaiTaskGroupClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pandai_task_group_finalize;
  object_class->get_property = pandai_task_group_get_property;
  object_class->set_property = pandai_task_group_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         "dummy",
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT |
                          G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_PRIORITY] =
    g_param_spec_enum ("priority", NULL, NULL,
                       PANDAI_TYPE_PRIORITY, PANDAI_PRIORITY_NORMAL,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                        G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
pandai_task_group_init (PandaiTaskGroup *self)
{
  self->task_list = g_ptr_array_new_with_free_func (g_object_unref);
  self->n_items = 0;
}


/**
 * pandai_task_group_new:
 * @name: a #gchar: the name of the group.
 *
 * Creates a new #PandaiTaskGroup.
 *
 * Returns: a newly created #PandaiTaskGroup or %NULL
 */
PandaiTaskGroup *
pandai_task_group_new (const gchar *name)
{
  if (!name)
  {
    g_critical ("Failed to create a new task group. Name must not be empty");
    return NULL;
  }

  return g_object_new (PANDAI_TYPE_TASK_GROUP,
                       "name", name,
                       NULL);
}

gchar *
pandai_task_group_get_name (PandaiTaskGroup *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK_GROUP (self), NULL);

  return self->name;
}

void
pandai_task_group_set_name (PandaiTaskGroup *self,
                            const gchar     *setting)
{
  g_return_if_fail (PANDAI_IS_TASK_GROUP (self));
  g_return_if_fail (setting != NULL);

  if (!pandai_str_equal (self->name, setting))
    {
      g_free (self->name);
      self->name = g_strdup (setting);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

PandaiPriority
pandai_task_group_get_priority (PandaiTaskGroup *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK_GROUP (self), PANDAI_PRIORITY_NORMAL);

  return self->priority;
}

void
pandai_task_group_set_priority (PandaiTaskGroup      *self,
                                const PandaiPriority  setting)
{
  g_return_if_fail (PANDAI_IS_TASK_GROUP (self));
  g_return_if_fail (PANDAI_IS_PRIORITY (setting));

  if (self->priority != setting)
    {
      self->priority = setting;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PRIORITY]);
    }
}



void
pandai_task_group_add_task (PandaiTaskGroup *self,
                            PandaiTask      *task)
{
  g_return_if_fail (PANDAI_IS_TASK_GROUP (self));
  g_return_if_fail (PANDAI_IS_TASK (task));

  g_ptr_array_add (self->task_list, task);
  g_list_model_items_changed (G_LIST_MODEL (self), self->n_items, 0, 1);
  g_atomic_int_inc (&self->n_items);
}

void
pandai_task_group_remove_task (PandaiTaskGroup *self,
                               PandaiTask      *task)
{
  guint index_ = 0;

  g_return_if_fail (PANDAI_IS_TASK_GROUP (self));
  g_return_if_fail (PANDAI_IS_TASK (task));

  if (!g_ptr_array_find (self->task_list, task, &index_))
    {
      g_critical ("Failed to remove task '%s' from Group '%s'. Task is not in the group.",
                  pandai_task_get_name (task), self->name);
      return;
    }

  if (g_ptr_array_remove (self->task_list, task))
    {
      g_list_model_items_changed (G_LIST_MODEL (self), index_, 1, 0);
      g_atomic_int_dec_and_test (&self->n_items);
    }
  else
    g_critical ("Failed to remove task '%s' from Group '%s'.",
                pandai_task_get_name (task), self->name);
}

/**
 * pandai_task_group_get_task:
 * @self: a #PandaiTaskGroup.
 * @index_: a #guint: the id of the task to get.
 *
 * Gets the @index_ th #PandaiTask of @self.
 *
 * Returns: (transfer full): A #PandaiTask or %NULL if the index is outside the group range.
 */
PandaiTask *
pandai_task_group_get_task (PandaiTaskGroup *self,
                            const guint      index_)
{
  g_return_val_if_fail (PANDAI_IS_TASK_GROUP (self), NULL);

  return (PandaiTask *)g_list_model_get_object (G_LIST_MODEL (self), index_);
}
