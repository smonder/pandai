/*
 * pandai-task.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiTask"

#include "pandai-config.h"
#include "pandai-debug.h"

#include "pandai-enums.h"
#include "pandai-macros.h"

#include "pandai-task.h"

struct _PandaiTask
{
  GObject          parent_instance;

  /* ----< TASK PROPERTIES >---- */
  gchar *          name;
  PandaiDateTime * date_start;
  PandaiDateTime * due_date;
  PandaiPriority   priority;
  PandaiStatus     status;

  /* ----< TASK DETAILED CONTENTS >---- */
  gchar *          memo;

  /* a placeholder for future media insertion ability */
  gpointer         media [12];
};

G_DEFINE_FINAL_TYPE (PandaiTask, pandai_task, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_DATE_START,
  PROP_DUE_DATE,
  PROP_PRIORITY,
  PROP_STATUS,
  PROP_MEMO,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
pandai_task_finalize (GObject *object)
{
  PandaiTask *self = (PandaiTask *)object;

  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->date_start, pandai_date_time_unref);
  g_clear_pointer (&self->due_date, pandai_date_time_unref);
  g_clear_pointer (&self->memo, g_free);

  G_OBJECT_CLASS (pandai_task_parent_class)->finalize (object);
}

static void
pandai_task_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  PandaiTask *self = PANDAI_TASK (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, pandai_task_get_name (self));
      break;

    case PROP_DATE_START:
      g_value_set_boxed (value, pandai_task_get_date_start (self));
      break;

    case PROP_DUE_DATE:
      g_value_set_boxed (value, pandai_task_get_due_date (self));
      break;

    case PROP_PRIORITY:
      g_value_set_enum (value, pandai_task_get_priority (self));
      break;

    case PROP_STATUS:
      g_value_set_enum (value, pandai_task_get_status (self));
      break;

    case PROP_MEMO:
      g_value_set_string (value, pandai_task_get_memo (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  PandaiTask *self = PANDAI_TASK (object);

  switch (prop_id)
    {
    case PROP_NAME:
      pandai_task_set_name (self, g_value_get_string (value));
      break;

    case PROP_DATE_START:
      pandai_task_set_date_start (self, g_value_get_boxed (value));
      break;

    case PROP_DUE_DATE:
      pandai_task_set_due_date (self, g_value_get_boxed (value));
      break;

    case PROP_PRIORITY:
      pandai_task_set_priority (self, g_value_get_enum (value));
      break;

    case PROP_STATUS:
      pandai_task_set_status (self, g_value_get_enum (value));
      break;

    case PROP_MEMO:
      pandai_task_set_memo (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_task_class_init (PandaiTaskClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pandai_task_finalize;
  object_class->get_property = pandai_task_get_property;
  object_class->set_property = pandai_task_set_property;

  properties [PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         "dummy",
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT |
                          G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_DATE_START] =
    g_param_spec_boxed ("date-start", NULL, NULL,
                        PANDAI_TYPE_DATE_TIME,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS));

  properties [PROP_DUE_DATE] =
    g_param_spec_boxed ("due-date", NULL, NULL,
                        PANDAI_TYPE_DATE_TIME,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS));

  properties [PROP_PRIORITY] =
    g_param_spec_enum ("priority", NULL, NULL,
                       PANDAI_TYPE_PRIORITY, PANDAI_PRIORITY_NORMAL,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                        G_PARAM_STATIC_STRINGS));

  properties [PROP_STATUS] =
    g_param_spec_enum ("status", NULL, NULL,
                       PANDAI_TYPE_STATUS, PANDAI_STATUS_NORMAL,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                        G_PARAM_STATIC_STRINGS));

  properties [PROP_MEMO] =
    g_param_spec_string ("memo", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));
}

static void
pandai_task_init (PandaiTask *self)
{
  self->date_start = pandai_date_time_new ();
  self->due_date = pandai_date_time_new ();
}


/**
 * pandai_task_new:
 * @name: a #gchar: the name of the task.
 *
 * Creates a new #PandaiTask.
 *
 * Returns: a newly created #PandaiTask or %NULL
 */
PandaiTask *
pandai_task_new (const gchar *name)
{
  if (!name)
    {
      g_critical ("Failed to create a new task. Name must not be empty");
      return NULL;
    }

  return g_object_new (PANDAI_TYPE_TASK,
                       "name", name,
                       NULL);
}


gchar *
pandai_task_get_name (PandaiTask *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK (self), NULL);

  return self->name;
}

void
pandai_task_set_name (PandaiTask  *self,
                      const gchar *setting)
{
  g_return_if_fail (PANDAI_IS_TASK (self));
  g_return_if_fail (setting != NULL);

  if (!pandai_str_equal (self->name, setting))
    {
      g_free (self->name);
      self->name = g_strdup (setting);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
    }
}

PandaiDateTime *
pandai_task_get_date_start (PandaiTask *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK (self), NULL);

  return self->date_start;
}

void
pandai_task_set_date_start (PandaiTask     *self,
                            PandaiDateTime *setting)
{
  g_return_if_fail (PANDAI_IS_TASK (self));

  if (self->date_start != setting)
    {
      pandai_date_time_unref (self->date_start);
      self->date_start = setting;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DATE_START]);
    }
}

PandaiDateTime *
pandai_task_get_due_date (PandaiTask *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK (self), NULL);

  return self->due_date;
}

void
pandai_task_set_due_date (PandaiTask     *self,
                          PandaiDateTime *setting)
{
  g_return_if_fail (PANDAI_IS_TASK (self));

  if (self->due_date != setting)
    {
      pandai_date_time_unref (self->due_date);
      self->due_date = setting;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DUE_DATE]);
    }
}

PandaiPriority
pandai_task_get_priority (PandaiTask *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK (self), PANDAI_PRIORITY_NORMAL);

  return self->priority;
}

void
pandai_task_set_priority (PandaiTask           *self,
                          const PandaiPriority  setting)
{
  g_return_if_fail (PANDAI_IS_TASK (self));
  g_return_if_fail (PANDAI_IS_PRIORITY (setting));

  if (self->priority != setting)
    {
      self->priority = setting;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PRIORITY]);
    }
}

PandaiStatus
pandai_task_get_status (PandaiTask *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK (self), PANDAI_STATUS_NORMAL);

  return self->status;
}

void
pandai_task_set_status (PandaiTask         *self,
                        const PandaiStatus  setting)
{
  g_return_if_fail (PANDAI_IS_TASK (self));
  g_return_if_fail (PANDAI_IS_STATUS (setting));

  if (self->status != setting)
    {
      self->status = setting;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_STATUS]);
    }
}


gchar *
pandai_task_get_memo (PandaiTask *self)
{
  g_return_val_if_fail (PANDAI_IS_TASK (self), NULL);

  return self->memo;
}

void
pandai_task_set_memo (PandaiTask  *self,
                      const gchar *setting)
{
  g_return_if_fail (PANDAI_IS_TASK (self));
  g_return_if_fail (setting != NULL);

  if (!pandai_str_equal (self->memo, setting))
    {
      g_free (self->memo);
      self->memo = g_strdup (setting);

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MEMO]);
    }
}
