/*
 * pandai-task.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(PANDAI_INSIDE) && !defined(PANDAI_COMPILATION)
#error "Only <libpandai.h> can be included directly."
#endif

#include <glib-object.h>

#include "pandai-datetime.h"
#include "pandai-version-macros.h"
#include "pandai-types.h"

G_BEGIN_DECLS

#define PANDAI_TYPE_TASK (pandai_task_get_type())

PANDAI_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (PandaiTask, pandai_task, PANDAI, TASK, GObject)

PANDAI_AVAILABLE_IN_ALL
PandaiTask *     pandai_task_new             (const gchar *        name) G_GNUC_WARN_UNUSED_RESULT;

PANDAI_AVAILABLE_IN_ALL
gchar *          pandai_task_get_name        (PandaiTask *         self);
PANDAI_AVAILABLE_IN_ALL
void             pandai_task_set_name        (PandaiTask *         self,
                                              const gchar *        setting);
PANDAI_AVAILABLE_IN_ALL
PandaiDateTime * pandai_task_get_date_start  (PandaiTask *         self);
PANDAI_AVAILABLE_IN_ALL
void             pandai_task_set_date_start  (PandaiTask *         self,
                                              PandaiDateTime *     setting);
PANDAI_AVAILABLE_IN_ALL
PandaiDateTime * pandai_task_get_due_date    (PandaiTask *         self);
PANDAI_AVAILABLE_IN_ALL
void             pandai_task_set_due_date    (PandaiTask *         self,
                                              PandaiDateTime *     setting);
PANDAI_AVAILABLE_IN_ALL
PandaiPriority   pandai_task_get_priority    (PandaiTask *         self);
PANDAI_AVAILABLE_IN_ALL
void             pandai_task_set_priority    (PandaiTask *         self,
                                              const PandaiPriority setting);
PANDAI_AVAILABLE_IN_ALL
PandaiStatus     pandai_task_get_status      (PandaiTask *         self);
PANDAI_AVAILABLE_IN_ALL
void             pandai_task_set_status      (PandaiTask *         self,
                                              const PandaiStatus   setting);

PANDAI_AVAILABLE_IN_ALL
gchar *          pandai_task_get_memo        (PandaiTask *         self);
PANDAI_AVAILABLE_IN_ALL
void             pandai_task_set_memo        (PandaiTask *         self,
                                              const gchar *        setting);


G_END_DECLS
