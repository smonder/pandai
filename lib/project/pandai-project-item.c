/*
 * pandai-project-item.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiProjectItem"

#include "pandai-config.h"
#include "pandai-debug.h"

#include "pandai-enums.h"
#include "pandai-macros.h"

#include "pandai-project-plan.h"

/**
 * PandaiProjectItem:
 *
 * This is the base type of all PandaiProject components.
 *
 */

#include "pandai-project-item.h"

typedef struct
{

} PandaiProjectItemPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (PandaiProjectItem, pandai_project_item, G_TYPE_OBJECT)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
pandai_project_item_finalize (GObject *object)
{
  PandaiProjectItem *self = (PandaiProjectItem *)object;
  PandaiProjectItemPrivate *priv = pandai_project_item_get_instance_private (self);
  G_OBJECT_CLASS (pandai_project_item_parent_class)->finalize (object);
}

static void
pandai_project_item_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  PandaiProjectItem *self = PANDAI_PROJECT_ITEM (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_project_item_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  PandaiProjectItem *self = PANDAI_PROJECT_ITEM (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_project_item_class_init (PandaiProjectItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pandai_project_item_finalize;
  object_class->get_property = pandai_project_item_get_property;
  object_class->set_property = pandai_project_item_set_property;
}

static void
pandai_project_item_init (PandaiProjectItem *self)
{
}
