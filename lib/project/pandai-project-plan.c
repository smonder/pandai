/*
 * pandai-project-plan.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiProjectPlan"

#include "pandai-config.h"
#include "pandai-debug.h"

#include "pandai-enums.h"
#include "pandai-macros.h"

#include "pandai-project-plan.h"

/**
 * PandaiProjectPlan:
 *
 * A project item that contains the plan and time schedule of the project.
 *
 * The `PandaiProjectPlan` is an important object in a Pandai Project because
 * it is responsible for organizing the project and it's time line and budget.
 *
 * The `PandaiProjectPlan` is a List of [class@Pandai.TaskGroup] objects
 * and [class@Pandai.Task] objects.
 *
 */


struct _PandaiProjectPlan
{
  PandaiProjectItem   parent_instance;

  /* ----< TASK_GROUP PROPERTIES >---- */

  /* ----< TASK_GROUP DETAILED CONTENTS >---- */
  GPtrArray *         group_list;
  guint               n_items;
};


static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_FINAL_TYPE_WITH_CODE (PandaiProjectPlan, pandai_project_plan, PANDAI_TYPE_PROJECT_ITEM,
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))


enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];
static GListModelInterface *parent_list_model_iface;

static GType
pandai_project_plan_get_item_type (GListModel *list)
{
  return PANDAI_TYPE_TASK;
}

static guint
pandai_project_plan_get_n_items (GListModel *list)
{
  PandaiProjectPlan *self = (PandaiProjectPlan *)list;

  g_assert (PANDAI_IS_PROJECT_PLAN (self));

  return self->n_items;
}

static gpointer
pandai_project_plan_get_item (GListModel *list,
                              guint       position)
{
  PandaiProjectPlan *self = (PandaiProjectPlan *)list;

  g_assert (PANDAI_IS_PROJECT_PLAN (self));

  return g_ptr_array_index (self->group_list, position);
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  parent_list_model_iface = g_type_interface_peek_parent (iface);

  iface->get_item_type = pandai_project_plan_get_item_type;
  iface->get_n_items = pandai_project_plan_get_n_items;
  iface->get_item = pandai_project_plan_get_item;
}



static void
pandai_project_plan_finalize (GObject *object)
{
  PandaiProjectPlan *self = (PandaiProjectPlan *)object;

  g_ptr_array_free (self->group_list, TRUE);

  G_OBJECT_CLASS (pandai_project_plan_parent_class)->finalize (object);
}

static void
pandai_project_plan_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  PandaiProjectPlan *self = PANDAI_PROJECT_PLAN (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_project_plan_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  PandaiProjectPlan *self = PANDAI_PROJECT_PLAN (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
pandai_project_plan_class_init (PandaiProjectPlanClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = pandai_project_plan_finalize;
  object_class->get_property = pandai_project_plan_get_property;
  object_class->set_property = pandai_project_plan_set_property;
}

static void
pandai_project_plan_init (PandaiProjectPlan *self)
{
  self->group_list = g_ptr_array_new_with_free_func (g_object_unref);
  self->n_items = 0;
}


/**
 * pandai_project_plan_new:
 *
 * Creates a new #PandaiProjectPlan object.
 *
 * Returns: a newly created #PandaiProjectPlan.
 */
PandaiProjectPlan *
pandai_project_plan_new (void)
{
  return g_object_new (PANDAI_TYPE_PROJECT_PLAN, NULL);
}


/**
 * pandai_project_plan_add_group:
 * @self: a #PandaiProjectPlan.
 * @group: a #PandaiTaskGroup.
 *
 * Add @group to @self at the index @index_.
 *
 */
void
pandai_project_plan_add_group (PandaiProjectPlan *self,
                               PandaiTaskGroup   *group,
                               const guint        index_)
{
  g_return_if_fail (PANDAI_IS_PROJECT_PLAN (self));
  g_return_if_fail (PANDAI_IS_TASK_GROUP (group));

  PANDAI_ENTRY;
  if (index_ >= self->n_items)
    {
      g_critical ("Unable to add group '%s' to project. Invalid index.",
                  pandai_task_group_get_name (group));
      PANDAI_EXIT;
    }

  g_ptr_array_insert (self->group_list, index_, group);
  g_list_model_items_changed (G_LIST_MODEL (self), index_, 0, 1);
  g_atomic_int_inc (&self->n_items);

  PANDAI_EXIT;
}

/**
 * pandai_project_plan_remove_group:
 * @self: a #PandaiProjectPlan.
 * @group: a #PandaiTaskGroup.
 *
 * Deletes @group from @self.
 *
 */
void
pandai_project_plan_remove_group (PandaiProjectPlan *self,
                                  PandaiTaskGroup   *group)
{
  guint index_ = 0;

  g_return_if_fail (PANDAI_IS_PROJECT_PLAN (self));
  g_return_if_fail (PANDAI_IS_TASK_GROUP (group));

  PANDAI_ENTRY;

  if (!g_ptr_array_find (self->group_list, group, &index_))
    {
      g_critical ("Failed to remove group '%s' from project. this task group is not in the plan.",
                  pandai_task_group_get_name (group));
      PANDAI_EXIT;
    }

  if (g_ptr_array_remove (self->group_list, group))
    {
      g_list_model_items_changed (G_LIST_MODEL (self), index_, 1, 0);
      g_atomic_int_dec_and_test (&self->n_items);
    }
  else
    g_critical ("Failed to remove group '%s' from project.",
                  pandai_task_group_get_name (group));

  PANDAI_EXIT;
}

/**
 * pandai_project_plan_get_group:
 * @self: a #PandaiProjectPlan.
 * @index_: a #guint: the id of the task group to get.
 *
 * Gets the @index_ th #PandaiTaskGroup of @self.
 *
 * Returns: (transfer full): A #PandaiTaskGroup or %NULL if the index is outside the plan range.
 */
PandaiTaskGroup *
pandai_project_plan_get_group (PandaiProjectPlan *self,
                               const guint        index_)
{
  g_return_val_if_fail (PANDAI_IS_PROJECT_PLAN (self), NULL);

  return (PandaiTaskGroup *)g_list_model_get_object (G_LIST_MODEL (self), index_);
}
