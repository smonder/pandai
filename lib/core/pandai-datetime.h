/*
 * pandai-datetime.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(PANDAI_INSIDE) && !defined(PANDAI_COMPILATION)
#error "Only <libpandai.h> can be included directly."
#endif

#include <glib-object.h>

#include "pandai-version-macros.h"
#include "pandai-types.h"

G_BEGIN_DECLS

#define PANDAI_TYPE_DATE_TIME (pandai_date_time_get_type ())

typedef struct _PandaiDateTime PandaiDateTime;



PANDAI_AVAILABLE_IN_ALL
GType               pandai_date_time_get_type       (void) G_GNUC_CONST;
PANDAI_AVAILABLE_IN_ALL
PandaiDateTime *    pandai_date_time_new            (void) G_GNUC_WARN_UNUSED_RESULT;
PANDAI_AVAILABLE_IN_ALL
PandaiDateTime *    pandai_date_time_copy           (PandaiDateTime *  self);
PANDAI_AVAILABLE_IN_ALL
PandaiDateTime *    pandai_date_time_ref            (PandaiDateTime *  self);
PANDAI_AVAILABLE_IN_ALL
void                pandai_date_time_unref          (PandaiDateTime *  self);
PANDAI_AVAILABLE_IN_ALL
void                pandai_date_time_set            (PandaiDateTime *  self,
                                                     PandaiDTComponent component,
                                                     const guint       value);
PANDAI_AVAILABLE_IN_ALL
guint               pandai_date_time_get            (PandaiDateTime *  self,
                                                     PandaiDTComponent  component);
PANDAI_AVAILABLE_IN_ALL
gchar *             pandai_date_time_get_formatted  (PandaiDateTime *  self);
PANDAI_AVAILABLE_IN_ALL
gboolean            pandai_date_time_set_formatted  (PandaiDateTime *  self,
                                                     const gchar *     formatted);
PANDAI_AVAILABLE_IN_ALL
gboolean            pandai_date_time_to_str_value   (GBinding *        binding,
                                                     const GValue *    pandai_dt,
                                                     GValue *          str_value,
                                                     gpointer          user_data);
PANDAI_AVAILABLE_IN_ALL
gboolean            pandai_date_time_from_str_value (GBinding *        binding,
                                                     const GValue *    str_value,
                                                     GValue *          pandai_dt,
                                                     gpointer          user_data);


G_DEFINE_AUTOPTR_CLEANUP_FUNC (PandaiDateTime, pandai_date_time_unref)

G_END_DECLS
