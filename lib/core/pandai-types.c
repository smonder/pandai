/*
 * pandai-types.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiTypes"

#include "pandai-config.h"
#include "pandai-debug.h"

#include "pandai-types.h"
#include "pandai-macros.h"

const gchar *
pandai_priority_to_icon_name (PandaiPriority p)
{
  switch (p)
    {
    case PANDAI_PRIORITY_NORMAL: return "pandai-priority-normal";
    case PANDAI_PRIORITY_LOW: return "pandai-priority-low";
    case PANDAI_PRIORITY_MEDIUM: return "pandai-priority-medium";
    case PANDAI_PRIORITY_HIGH: return "pandai-priority-high";
    case PANDAI_PRIORITY_URGENT: return "pandai-priority-urgent";
    default: return "pandai-priority-normal";
    }
}

gboolean
pandai_priority_to_icon_value (GBinding     *binding,
                               const GValue *pandai_priority,
                               GValue       *icon_name,
                               gpointer      user_data)
{
  PandaiPriority v = g_value_get_enum (pandai_priority);

  if (!G_VALUE_HOLDS_ENUM (pandai_priority) || !PANDAI_IS_PRIORITY (v))
    return FALSE;

  g_value_set_string (icon_name,
                      pandai_priority_to_icon_name (v));

  return TRUE;
}

const gchar *
pandai_status_to_icon_name (PandaiStatus s)
{
  switch (s)
    {
    case PANDAI_STATUS_NORMAL: return "pandai-priority-normal";
    case PANDAI_STATUS_FINISHED: return "pandai-status-finished";
    case PANDAI_STATUS_POSTPONED: return "pandai-status-postponed";
    case PANDAI_STATUS_WORKING: return "pandai-status-working";
    default: return "pandai-priority-normal";
    }
}

gboolean
pandai_status_to_icon_value (GBinding     *binding,
                             const GValue *pandai_status,
                             GValue       *icon_name,
                             gpointer      user_data)
{
  PandaiStatus s = g_value_get_enum (pandai_status);

  if (!G_VALUE_HOLDS_ENUM (pandai_status) || !PANDAI_IS_STATUS (s))
    return FALSE;

  g_value_set_string (icon_name,
                      pandai_status_to_icon_name (s));

  return TRUE;
}
