/*
 * pandai-types.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(PANDAI_INSIDE) && !defined(PANDAI_COMPILATION)
#error "Only <libpandai.h> can be included directly."
#endif

#include <glib-object.h>
#include "pandai-version-macros.h"

G_BEGIN_DECLS

#define PANDAI_IS_PRIORITY(x) (((x) <= PANDAI_PRIORITY_NORMAL))
#define PANDAI_IS_STATUS(x) (((x) <= PANDAI_STATUS_NORMAL))

typedef enum {
  PANDAI_PRIORITY_LOW,
  PANDAI_PRIORITY_MEDIUM,
  PANDAI_PRIORITY_HIGH,
  PANDAI_PRIORITY_URGENT,
  PANDAI_PRIORITY_NORMAL
} PandaiPriority;

typedef enum {
  PANDAI_STATUS_POSTPONED,
  PANDAI_STATUS_WORKING,
  PANDAI_STATUS_FINISHED,
  PANDAI_STATUS_NORMAL
} PandaiStatus;

typedef enum {
  PANDAI_DT_DAY,
  PANDAI_DT_MONTH,
  PANDAI_DT_YEAR,
  PANDAI_DT_HOUR,
  PANDAI_DT_MINUTE,
  PANDAI_DT_SECOND
} PandaiDTComponent;



PANDAI_AVAILABLE_IN_ALL
const gchar * pandai_priority_to_icon_name    (PandaiPriority p);
PANDAI_AVAILABLE_IN_ALL
gboolean      pandai_priority_to_icon_value   (GBinding *       binding,
                                               const GValue *   pandai_priority,
                                               GValue *         icon_name,
                                               gpointer         user_data);

PANDAI_AVAILABLE_IN_ALL
const gchar * pandai_status_to_icon_name      (PandaiStatus     s);
PANDAI_AVAILABLE_IN_ALL
gboolean      pandai_status_to_icon_value     (GBinding *       binding,
                                               const GValue *   pandai_status,
                                               GValue *         icon_name,
                                               gpointer         user_data);

G_END_DECLS
