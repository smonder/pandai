/*
 * pandai-datetime.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiDateTime"

#include "pandai-config.h"
#include "pandai-debug.h"

#include "pandai-macros.h"

#include <stdio.h>

#include "pandai-datetime.h"

struct _PandaiDateTime
{
  guint day;
  guint month;
  guint year;

  guint hour;
  guint minute;
  guint second;

  /*< private >*/
  guint ref_count;
};

G_DEFINE_BOXED_TYPE (PandaiDateTime, pandai_date_time, pandai_date_time_ref, pandai_date_time_unref)

/**
 * pandai_date_time_new:
 *
 * Creates a new #PandaiDateTime.
 *
 * Returns: (transfer full): A newly created #PandaiDateTime
 */
PandaiDateTime *
pandai_date_time_new (void)
{
  PandaiDateTime *self;
  g_autoptr (GDateTime) now = g_date_time_new_now_local ();

  self = g_slice_new0 (PandaiDateTime);
  self->ref_count = 1;

  self->year = g_date_time_get_year (now);
  self->month = g_date_time_get_month (now);
  self->day = g_date_time_get_day_of_month (now);
  self->hour = g_date_time_get_hour (now);
  self->minute = g_date_time_get_minute (now);
  self->second = g_date_time_get_second (now);

  return self;
}

/**
 * pandai_date_time_copy:
 * @self: a #PandaiDateTime
 *
 * Makes a deep copy of a #PandaiDateTime.
 *
 * Returns: (transfer full): A newly created #PandaiDateTime with the same
 *   contents as @self
 */
PandaiDateTime *
pandai_date_time_copy (PandaiDateTime *self)
{
  PandaiDateTime *copy;

  g_return_val_if_fail (self, NULL);
  g_return_val_if_fail (self->ref_count, NULL);

  copy = pandai_date_time_new ();
  copy->year = self->year;
  copy->month = self->month;
  copy->day = self->day;
  copy->hour = self->hour;
  copy->minute = self->minute;
  copy->second = self->second;

  return copy;
}

static void
pandai_date_time_free (PandaiDateTime *self)
{
  g_assert (self);

  g_assert_cmpint (self->ref_count, ==, 0);

  g_slice_free (PandaiDateTime, self);
}

/**
 * pandai_date_time_ref:
 * @self: A #PandaiDateTime
 *
 * Increments the reference count of @self by one.
 *
 * Returns: (transfer full): @self
 */
PandaiDateTime *
pandai_date_time_ref (PandaiDateTime *self)
{
  g_return_val_if_fail (self, NULL);
  g_return_val_if_fail (self->ref_count, NULL);

  g_atomic_int_inc (&self->ref_count);

  return self;
}

/**
 * pandai_date_time_unref:
 * @self: A #PandaiDateTime
 *
 * Decrements the reference count of @self by one, freeing the structure when
 * the reference count reaches zero.
 */
void
pandai_date_time_unref (PandaiDateTime *self)
{
  g_return_if_fail (self);
  g_return_if_fail (self->ref_count);

  if (g_atomic_int_dec_and_test (&self->ref_count))
    pandai_date_time_free (self);
}


guint
pandai_date_time_get (PandaiDateTime    *self,
                      PandaiDTComponent  component)
{
  g_return_val_if_fail (self, 0);
  g_return_val_if_fail (self->ref_count, 0);

  switch (component)
    {
    case PANDAI_DT_DAY: return self->day;
    case PANDAI_DT_MONTH: return self->month;
    case PANDAI_DT_YEAR: return self->year;
    case PANDAI_DT_HOUR: return self->hour;
    case PANDAI_DT_MINUTE: return self->minute;
    case PANDAI_DT_SECOND: return self->second;
    default: return 0;
    }
}

void
pandai_date_time_set (PandaiDateTime    *self,
                      PandaiDTComponent  component,
                      const guint        value)
{
  g_return_if_fail (self);
  g_return_if_fail (self->ref_count);

  switch (component)
    {
    case PANDAI_DT_DAY:
      self->day = value;
      break;

    case PANDAI_DT_MONTH:
      self->month = value;
      break;

    case PANDAI_DT_YEAR:
      self->year = value;
      break;

    case PANDAI_DT_HOUR:
      self->hour = value;
      break;

    case PANDAI_DT_MINUTE:
      self->minute = value;
      break;

    case PANDAI_DT_SECOND:
      self->second = value;
      break;

    default: break;
    }
}

gchar *
pandai_date_time_get_formatted (PandaiDateTime *self)
{
  g_return_val_if_fail (self, NULL);
  g_return_val_if_fail (self->ref_count, NULL);

  return g_strdup_printf ("%d/%d/%d - %d:%d:%d",
                          self->day, self->month, self->year,
                          self->hour, self->minute, self->second);
}

gboolean
pandai_date_time_set_formatted (PandaiDateTime *self,
                                const gchar    *format)
{
  g_return_val_if_fail (self, FALSE);
  g_return_val_if_fail (self->ref_count, FALSE);
  g_return_val_if_fail (format != NULL, FALSE);

  return sscanf (format,
                 "%d/%d/%d - %d:%d:%d",
                 &self->day, &self->month, &self->year,
                 &self->hour, &self->minute, &self->second);
}

gboolean
pandai_date_time_to_str_value   (GBinding     *binding,
                                 const GValue *pandai_date_time,
                                 GValue       *str_value,
                                 gpointer      user_data)
{
  if (!G_VALUE_HOLDS_BOXED (pandai_date_time))
    return FALSE;

  g_value_set_string (str_value,
                      pandai_date_time_get_formatted ((PandaiDateTime *)g_value_get_boxed (pandai_date_time)));

  return TRUE;
}

gboolean
pandai_date_time_from_str_value (GBinding     *binding,
                                 const GValue *str_value,
                                 GValue       *pandai_date_time,
                                 gpointer      user_data)
{
  PandaiDateTime *bdt;

  if (!G_VALUE_HOLDS_STRING (str_value))
    return FALSE;

  bdt = pandai_date_time_new ();

  if (! pandai_date_time_set_formatted (bdt, g_value_get_string (str_value)))
    return FALSE;

  g_value_set_boxed (pandai_date_time, bdt);
  return TRUE;
}
