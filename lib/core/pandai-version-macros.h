/* pandai-version-macros.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(PANDAI_INSIDE) && !defined(PANDAI_COMPILATION)
#error "Only <libpandai.h> can be included directly."
#endif

#include <glib.h>

#include "pandai-version.h"

#ifndef _PANDAI_EXTERN
#define _PANDAI_EXTERN extern
#endif

#ifdef PANDAI_DISABLE_DEPRECATION_WARNINGS
# define PANDAI_DEPRECATED _PANDAI_EXTERN
# define PANDAI_DEPRECATED_FOR(f) _PANDAI_EXTERN
# define PANDAI_UNAVAILABLE(maj,min) _PANDAI_EXTERN
#else
# define PANDAI_DEPRECATED G_DEPRECATED _PANDAI_EXTERN
# define PANDAI_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _PANDAI_EXTERN
# define PANDAI_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _PANDAI_EXTERN
#endif

#define PANDAI_VERSION_1_0 (G_ENCODE_VERSION (1, 0))

#if (PANDAI_MINOR_VERSION == 99)
# define PANDAI_VERSION_CUR_STABLE (G_ENCODE_VERSION (PANDAI_MAJOR_VERSION + 1, 0))
#elif (PANDAI_MINOR_VERSION % 2)
# define PANDAI_VERSION_CUR_STABLE (G_ENCODE_VERSION (PANDAI_MAJOR_VERSION, PANDAI_MINOR_VERSION + 1))
#else
# define PANDAI_VERSION_CUR_STABLE (G_ENCODE_VERSION (PANDAI_MAJOR_VERSION, PANDAI_MINOR_VERSION))
#endif

#if (PANDAI_MINOR_VERSION == 99)
# define PANDAI_VERSION_PREV_STABLE (G_ENCODE_VERSION (PANDAI_MAJOR_VERSION + 1, 0))
#elif (PANDAI_MINOR_VERSION % 2)
# define PANDAI_VERSION_PREV_STABLE (G_ENCODE_VERSION (PANDAI_MAJOR_VERSION, PANDAI_MINOR_VERSION - 1))
#else
# define PANDAI_VERSION_PREV_STABLE (G_ENCODE_VERSION (PANDAI_MAJOR_VERSION, PANDAI_MINOR_VERSION - 2))
#endif

/**
 * PANDAI_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the libpandai.h header.
 *
 * The definition should be one of the predefined PANDAI version
 * macros: %PANDAI_VERSION_1_0, ...
 *
 * This macro defines the lower bound for the PANDAI API to use.
 *
 * If a function has been deprecated in a newer version of PANDAI,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 */
#ifndef PANDAI_VERSION_MIN_REQUIRED
# define PANDAI_VERSION_MIN_REQUIRED (PANDAI_VERSION_CUR_STABLE)
#endif

/**
 * PANDAI_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the libpandai.h header.

 * The definition should be one of the predefined PANDAI version
 * macros: %PANDAI_VERSION_1_0, %PANDAI_VERSION_1_2,...
 *
 * This macro defines the upper bound for the PANDAI API to use.
 *
 * If a function has been introduced in a newer version of PANDAI,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 */
#ifndef PANDAI_VERSION_MAX_ALLOWED
# if PANDAI_VERSION_MIN_REQUIRED > PANDAI_VERSION_PREV_STABLE
#  define PANDAI_VERSION_MAX_ALLOWED (PANDAI_VERSION_MIN_REQUIRED)
# else
#  define PANDAI_VERSION_MAX_ALLOWED (PANDAI_VERSION_CUR_STABLE)
# endif
#endif

#if PANDAI_VERSION_MAX_ALLOWED < PANDAI_VERSION_MIN_REQUIRED
#error "PANDAI_VERSION_MAX_ALLOWED must be >= PANDAI_VERSION_MIN_REQUIRED"
#endif
#if PANDAI_VERSION_MIN_REQUIRED < PANDAI_VERSION_1_0
#error "PANDAI_VERSION_MIN_REQUIRED must be >= PANDAI_VERSION_1_0"
#endif

#define PANDAI_AVAILABLE_IN_ALL                  _PANDAI_EXTERN

#if PANDAI_VERSION_MIN_REQUIRED >= PANDAI_VERSION_1_0
# define PANDAI_DEPRECATED_IN_1_0                PANDAI_DEPRECATED
# define PANDAI_DEPRECATED_IN_1_0_FOR(f)         PANDAI_DEPRECATED_FOR(f)
#else
# define PANDAI_DEPRECATED_IN_1_0                _PANDAI_EXTERN
# define PANDAI_DEPRECATED_IN_1_0_FOR(f)         _PANDAI_EXTERN
#endif

#if PANDAI_VERSION_MAX_ALLOWED < PANDAI_VERSION_1_0
# define PANDAI_AVAILABLE_IN_1_0                 PANDAI_UNAVAILABLE(1, 0)
#else
# define PANDAI_AVAILABLE_IN_1_0                 _PANDAI_EXTERN
#endif
