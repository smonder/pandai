/*
 * libpandai.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#pragma once

#define PANDAI_INSIDE

#include "pandai-debug.h"
#include "pandai-enums.h"
#include "pandai-version.h"

/* Application */
#include "application/pandai-application.h"

/* Core */
#include "core/pandai-macros.h"
#include "core/pandai-types.h"
#include "core/pandai-version-macros.h"

/* Project */
#include "project/pandai-project-item.h"
#include "project/pandai-project-plan.h"
#include "project/pandai-task.h"
#include "project/pandai-task-group.h"

/* Widgets */
#include "widgets/pandai-task-viewer.h"

#undef PANDAI_INSIDE
