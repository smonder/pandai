/* pandai-application.c
 *
 * Copyright 2023 Salim Monder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiApplication"

#include "pandai-config.h"
#include "pandai-debug.h"

#include "pandai-application-private.h"

G_DEFINE_TYPE (PandaiApplication, pandai_application, ADW_TYPE_APPLICATION)

static GApplicationFlags flags = G_APPLICATION_DEFAULT_FLAGS;
static GSettings *settings;


static gboolean
style_variant_to_color_scheme (GValue   *value,
                               GVariant *variant,
                               gpointer  user_data)
{
  const gchar *str = g_variant_get_string (variant, NULL);

  if (pandai_str_equal (str, "dark"))
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_DARK);
  else if (pandai_str_equal (str, "light"))
    g_value_set_enum (value, ADW_COLOR_SCHEME_FORCE_LIGHT);
  else
    g_value_set_enum (value, ADW_COLOR_SCHEME_DEFAULT);

  return TRUE;
}


static void
pandai_application_startup (GApplication *app)
{
  PandaiApplication *self = (PandaiApplication *)app;
  AdwStyleManager *style_manager;
  g_autoptr(GAction) theme = NULL;

  g_assert (PANDAI_IS_APPLICATION (app));

  theme = g_settings_create_action (settings, "style-variant");
  g_action_map_add_action (G_ACTION_MAP (self), theme);

  G_APPLICATION_CLASS (pandai_application_parent_class)->startup (app);

  style_manager = adw_style_manager_get_default ();
  g_settings_bind_with_mapping (settings, "style-variant",
                                style_manager, "color-scheme",
                                G_SETTINGS_BIND_GET,
                                style_variant_to_color_scheme,
                                NULL, NULL, NULL);
}

static void
pandai_application_activate (GApplication *app)
{
	GtkWindow *window;

	g_assert (PANDAI_IS_APPLICATION (app));

	window = gtk_application_get_active_window (GTK_APPLICATION (app));

	if (window == NULL)
		window = g_object_new (PANDAI_TYPE_WINDOW,
		                       "application", app,
                           "title", PACKAGE_NAME,
		                       NULL);

	gtk_window_present (window);
}

static void
pandai_application_window_added (GtkApplication *app,
                                 GtkWindow      *window)
{
  g_assert (ADW_IS_APPLICATION (app));
  g_assert (GTK_IS_WINDOW (window));

#if DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (window), "devel");
#endif

  GTK_APPLICATION_CLASS (pandai_application_parent_class)->window_added (app, window);
}

static void
pandai_application_class_init (PandaiApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);
  GtkApplicationClass *gtk_app_class = GTK_APPLICATION_CLASS (klass);

  app_class->activate = pandai_application_activate;
  app_class->startup = pandai_application_startup;
  gtk_app_class->window_added = pandai_application_window_added;

  settings = g_settings_new (GSETTINGS_SCHEME_ID);
}

static void
pandai_application_init (PandaiApplication *self)
{
  _pandai_application_init_actions (self);
}


/**
 * pandai_application_new:
 *
 * Creates a new #PandaiApplication
 *
 * Returns: a newly created #PandaiApplication
 */
PandaiApplication *
pandai_application_new (void)
{
  return g_object_new (PANDAI_TYPE_APPLICATION,
                       "application-id", APP_ID,
	                     "flags", flags,
                       "resource-base-path", "/io/sam/pandai-app",
	                     NULL);
}
