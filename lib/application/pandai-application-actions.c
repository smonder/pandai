/*
 * pandai-application-actions.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiAppActions"

#include "pandai-config.h"
#include "pandai-debug.h"

#include "pandai-version.h"

#include "pandai-application-private.h"
#include "pandai-debug-info.h"


static void          pandai_application_about_action     (GSimpleAction * action,
                                                          GVariant *      parameter,
                                                          gpointer        user_data);
static void          pandai_application_quit_action      (GSimpleAction * action,
                                                          GVariant *      parameter,
                                                          gpointer        user_data);


static const GActionEntry app_actions[] = {
	{ "quit", pandai_application_quit_action },
	{ "about", pandai_application_about_action },
};

void
_pandai_application_init_actions (PandaiApplication *self)
{
  g_assert (PANDAI_IS_APPLICATION (self));

  g_action_map_add_action_entries (G_ACTION_MAP (self),
	                                 app_actions,
	                                 G_N_ELEMENTS (app_actions),
	                                 self);
	gtk_application_set_accels_for_action (GTK_APPLICATION (self),
	                                       "app.quit",
	                                       (const char *[]) { "<primary>q", NULL });
}


static void
pandai_application_about_action (GSimpleAction *action,
                                 GVariant      *parameter,
                                 gpointer       user_data)
{
  static const char *developers[] = {"Salim Monder", NULL};
  PandaiApplication *self = user_data;
  GtkWindow *window = NULL;

  g_assert (PANDAI_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  adw_show_about_window (window,
                         "application-name", PACKAGE_NAME,
                         "application-icon", "io.sam.pandai",
                         "developer-name", "Salim Monder",
                         "version", PANDAI_VERSION_S,
                         "developers", developers,
                         "copyright", "© 2023 Salim Monder",
                         "debug-info", _pandai_generate_debug_info (),
                         "debug-info-filename", "pandai-debug-info.txt",
                         NULL);
}

static void
pandai_application_quit_action (GSimpleAction *action,
                                GVariant      *parameter,
                                gpointer       user_data)
{
  PandaiApplication *self = user_data;

  g_assert (PANDAI_IS_APPLICATION (self));

  g_application_quit (G_APPLICATION (self));
}
