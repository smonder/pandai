/* pandai-window.c
 *
 * Copyright 2023 Salim Monder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "PandaiWindow"

#include "pandai-config.h"
#include "pandai-debug.h"

#include <glib/gi18n.h>

#include "pandai-macros.h"
#include "pandai-window.h"

struct _PandaiWindow
{
	AdwApplicationWindow  parent_instance;

	/* Template widgets */
	AdwHeaderBar *       header_bar;
	AdwStatusPage *      status;
};

G_DEFINE_FINAL_TYPE (PandaiWindow, pandai_window, ADW_TYPE_APPLICATION_WINDOW)

static GSettings * settings;


static void
load_geometry (PandaiWindow *self)
{
  GtkWindow *window;
  gboolean maximized;
  gint height;
  gint width;

  g_assert (PANDAI_IS_WINDOW (self));

  window = GTK_WINDOW (self);

  maximized = g_settings_get_boolean (settings, "window-maximized");
  g_settings_get (settings, "window-size", "(ii)", &width, &height);

  gtk_window_set_default_size (window, width, height);

  if (maximized)
    gtk_window_maximize (window);
}

static void
pandai_window_unmap (GtkWidget *widget)
{
  PandaiWindow *self = (PandaiWindow *)widget;
  gboolean maximized;

  maximized = gtk_window_is_maximized (GTK_WINDOW (self));
  g_settings_set_boolean (settings, "window-maximized", maximized);

  if (!maximized)
    {
      gint height;
      gint width;

      gtk_window_get_default_size (GTK_WINDOW (self), &width, &height);
      g_settings_set (settings, "window-size", "(ii)", width, height);
    }

  GTK_WIDGET_CLASS (pandai_window_parent_class)->unmap (widget);
}

static void
pandai_window_constructed (GObject *object)
{
  PandaiWindow *self = (PandaiWindow *)object;

  G_OBJECT_CLASS (pandai_window_parent_class)->constructed (object);

  load_geometry (self);
}

static void
pandai_window_class_init (PandaiWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = pandai_window_constructed;

  widget_class->unmap = pandai_window_unmap;
  gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/pandai-app/pandai-window.ui");
  gtk_widget_class_bind_template_child (widget_class, PandaiWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, PandaiWindow, status);

  settings = g_settings_new (GSETTINGS_SCHEME_ID);
}

static void
pandai_window_init (PandaiWindow *self)
{
	gtk_widget_init_template (GTK_WIDGET (self));
}
